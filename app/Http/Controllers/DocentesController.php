<?php

namespace App\Http\Controllers;

use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\DocenteRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DocentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $termino = "";
            if ($request->has('termino')) {
                $termino = $request->input('termino');
            }
            $limit = $request->input('per_page');
            //TRAER LOS DOCENTES DEL CACHE
            $cacheKey = 'docentes.all.'.$request->input('page').$termino;
            if(Cache::has('docentes')){
                $docentesCache = Cache::get('docentes');
                if(!array_key_exists($cacheKey, $docentesCache)){
                    $docentesCache[$cacheKey] = User::query()->buscarCoincidencia($termino)
                        ->where('role', 'Docente')
                        ->orderBy('name')
                        ->paginate($limit);
                }
            }else{
                $docentesCache = [];
                $docentesCache[$cacheKey] = User::query()->buscarCoincidencia($termino)
                    ->where('role', 'Docente')
                    ->orderBy('name')
                    ->paginate($limit);
            }
            Cache::forever('docentes',$docentesCache);
            $docentes = $docentesCache[$cacheKey];
            return Response::responseSuccess('', CodesResponse::CODE_OK, $docentes);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocenteRequest $request)
    {
        try{
            $docente = new User();
            $docente->fill($request->all());
            $docente->role = 'Docente';
            $docente->password = bcrypt($request->identificacion);
            $docente->save();
            Cache::forget('docentes');
            return Response::responseSuccess('El docente se creó correctamente.',CodesResponse::CODE_OK, $docente);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $docente = User::query()->findOrFail($id);
            return Response::responseSuccess('',CodesResponse::CODE_OK,$docente);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_BAD_REQUEST);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocenteRequest $request, $id)
    {
        try{
            $docente = User::query()->findOrFail($id);
            $docente->fill($request->all());
            $docente->save();
            Cache::forget('docentes');
            return Response::responseSuccess('El docente se actualizó correctamente',CodesResponse::CODE_OK,$docente);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $docente = User::query()->with('asignaturas')->findOrFail($id);
            if($docente->asignaturas->count() > 0){
                //Esto es solo para mostrar un error bonito
                $str = '';
                for($i=0; $i<$docente->asignaturas->count();$i++){
                    if($i<$docente->asignaturas->count()-1) {
                        $str .= $docente->asignaturas[$i]->nombre . ', ';
                    }else{
                        $str .= 'y '.$docente->asignaturas[$i]->nombre.'.';
                    }
                }
                return Response::responseError('No puedes borrar el docente, ya que está relacionado con las asignaturas '.$str,
                    CodesResponse::CODE_BAD_REQUEST);
            }else{
                $docente->delete();
                Cache::forget('docentes');
                return Response::responseSuccess('El docente se eliminó correctamente',CodesResponse::CODE_OK,$docente);
            }
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_BAD_REQUEST);
        }
    }
}
