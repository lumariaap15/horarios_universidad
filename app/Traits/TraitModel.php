<?php

namespace App\Traits;

trait TraitModel{


    public function scopeBuscarCoincidencia($query, $termino){
        foreach ($this->filasBuscar as $fila){
            $query->orWhere($fila,'like','%'.$termino.'%');
        }
        return $query;
    }

}
