<?php

use App\Asignatura;
use App\Aula;
use App\Clase;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatosPruebaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ASIGNATURAS
        Asignatura::query()->create([
            'nombre'=>'Metodología de la Educación',
            'codigo'=>'HGE837',
            'docente_id'=>1,
        ]);
        Asignatura::query()->create([
            'nombre'=>'Teoría General de Sistemas',
            'codigo'=>'32442',
            'docente_id'=>1,
        ]);
        Asignatura::query()->create([
            'nombre'=>'Matemáticas 1',
            'codigo'=>'7765',
            'docente_id'=>1,
        ]);
        Asignatura::query()->create([
            'nombre'=>'Inglés 2',
            'codigo'=>'154453',
            'docente_id'=>1,
        ]);
        $arr = [1,2,3,4];
        $estudiantes = User::query()->where('role','Estudiante')->get();
        foreach ($estudiantes as $estudiante){
            $estudiante->asignaturas()->attach([1,4,random_int(2,3)]);
        }

        //AULAS
        Aula::query()->create([
            'nombre'=>'Sala de sistemas'
        ]);
        Aula::query()->create([
            'nombre'=>'306'
        ]);
        Aula::query()->create([
            'nombre'=>'307'
        ]);

        //CLASES
        $horas = array("9:00:00","11:00:00","7:00:00","2:00:00");
        $aulas = [1,2,3];
        for($i=0;$i<10;$i++) {
            Clase::query()->create([
                'fecha' => Carbon::now()->addDays($i),
                'hora' => $horas[(array_rand($horas))],
                'asignatura_id' => $arr[array_rand($arr)],
                'aula_id' => $aulas[array_rand($aulas)]
            ]);
        }
    }
}
