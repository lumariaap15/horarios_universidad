<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(\App\User::class, 1)->create(['role'=>'Administrador']);
        factory(\App\User::class, 11)->create(['role'=>'Docente']);
        factory(\App\User::class, 11)->create(['role'=>'Estudiante']);
        $this->call(DatosPruebaSeeder::class);
    }
}
