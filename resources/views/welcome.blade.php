<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>HorariosUSTA</title>
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <principal-component></principal-component>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
