import VueRouter from 'vue-router'
import AdminIndex from './pages/admin/index';
import AdminDocentes from './pages/admin/docentes/docentes';
import AdminAsignaturas from './pages/admin/asignaturas/asignaturas';
import AdminClases from './pages/admin/clases/clases';
import AdminConfig from './pages/admin/configuracion/configuracion';
import AdminEstudiantes from './pages/admin/estudiantes/estudiantes';
import AdminSalones from './pages/admin/salones/salones';

import layout from "./pages/layout";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {path:'/', redirect: '/admin'},
        {
            path: '/admin',
            component: layout,
            children:[
                {
                    path:'',
                    component: AdminIndex,
                    name:'admin-index'
                },
                {
                    path:'docentes',
                    component: AdminDocentes,
                    name:'admin-docentes'
                },
                {
                    path:'estudiantes',
                    component: AdminEstudiantes,
                    name:'admin-estudiantes'
                },
                {
                    path:'asignaturas',
                    component: AdminAsignaturas,
                    name:'admin-asignaturas'
                },
                {
                    path:'salones',
                    component: AdminSalones,
                    name:'admin-salones'
                },
                {
                    path:'clases',
                    component: AdminClases,
                    name:'admin-clases'
                },
                {
                    path:'configuracion',
                    component: AdminConfig,
                    name:'admin-configuracion'
                },
            ]
        },
    ]
});


export default router;
