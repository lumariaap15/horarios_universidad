import axios from './GlobalService';

const DocentesService = {
    all(query) {
        return axios.get('docentes'+query);
    },
    store(data) {
        return axios.post('docentes', data);
    },
    find(id) {
        return axios.get(`docentes/${id}`);
    },
    update(id, data) {
        return axios.put(`docentes/${id}`, data);
    },
    delete(id) {
        return axios.delete(`docentes/${id}`);
    },
};

export default DocentesService;
