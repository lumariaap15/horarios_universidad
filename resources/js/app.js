require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'


Vue.component('principal-component', require('./components/PrincipalComponent.vue').default);
Vue.use(VueRouter);

import VueSwal from 'vue-swal'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(VueSwal)

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

//CONFIGURACIONES VEE-VALIDATE
import { ValidationProvider, extend, ValidationObserver } from 'vee-validate';

// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

import * as rules from 'vee-validate/dist/rules';
import es from 'vee-validate/dist/locale/es';

// loop over all rules
for (let rule in rules) {
    extend(rule, {
        ...rules[rule], // add the rule
        message: es.messages[rule] // add its message
    });
}

// Import component
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.component('Loading',Loading);

Vue.mixin({
    methods:{
        alertaSuccess(action, callback){
            this.$swal({
                title: "Bien!!",
                text: this.alertaLabel+' se '+action+' correctamente.',
                icon: "success",
            }).then(()=>{
                callback()
            });
        }
    }
});


import router from './router.js';

const app = new Vue({
    el: '#app',
    router
});
